![randompic logo](http://i.imgur.com/U8zdyxZ.png)

# What is it? #

**randompic** добавляет рандомную пикчу к каждому создаваемому на [2ch.hk](https://2ch.hk/) треду или посту, который содержит слово 'randompic', 'рандомпик' или 'пикрандом' (в любом регистре)

![example](http://i.imgur.com/s9IHmNj.png)

### How can I help? ###

* Fork
* Make changes
* Create pull request

### How can I contact the author? ###

Send me an e-mail to b0r3d0mness [at] gmail [dot] com
