// Let's duplicate few functions from "makaba/swag.js"
// We can't call the original ones directly even from the page-related environment
// because they were defined inside a function

// =====================================
// From makaba/swag.js
// Code style etc remain intact
// =====================================

var request;
var busy = false;
var qr = $('#qr');
var forms =  $('#postform,#qr-postform');
var submit_buttons = $('#qr-submit,#submit');

var renderSending = function(){
    /*var inputs = forms.find('input,select,textarea').not('[type=submit]');
    inputs.attr('disabled','disabled');*/
    submit_buttons.attr('value', 'Отправка...');
};

var renderSendingDone = function(){
    /*var inputs = forms.find('input,select,textarea').not('[type=submit]');
    inputs.removeAttr('disabled');*/
    submit_buttons.attr('value', 'Отправить');
};

var progressHandling = function(e) {
    var percent = 100/e.total*e.loaded;
    if(percent >= 99) return submit_buttons.attr('value', 'Обработка...');

    var bpercent = ( (Math.round(percent*100))/100 ).toString().split('.');
    if(!bpercent[1]) bpercent[1] = 0;
    bpercent = (bpercent[0].length==1?'0'+bpercent[0]:bpercent[0]) + '.' + (bpercent[1].length==1?bpercent[1]+'0':bpercent[1]);

    $('#qr-progress-bar').attr('value', e.loaded).attr('max', e.total);
    submit_buttons.attr('value', bpercent + '%');
};

var on_send_error = function(request) {
    if(request.statusText == 'abort') {
        $alert('Отправка сообщения отменена');
    }else{
        $alert('Ошибка постинга: ' + request.statusText);
    }

    on_complete();
};

var on_send_success = function(data) {
    if(data.Error) {
        if(data.Id) {
            $alert(data.Reason + '<br><a href="/ban?Id=' + data.Id + '" target="_blank">Подробнее</a>', 'wait');
        }else{
            $alert('Ошибка постинга: ' + (data.Reason || data.Error));
            if(data.Error == -5) window.postform_validator_error('captcha-value');
        }
    }else if(data.Status && data.Status == 'OK') {
        $alert('Сообщение успешно отправлено');
        if(Store.get('other.qr_close_on_send', true)) $('#qr').hide();

        if(!window.thread.id) { //костыль
            var behavior = Store.get('other.on_reply_from_main', 1);
            if(behavior == 1) {
                window.location.href = '/' + window.board + '/res/' + $('#qr-thread').val() + '.html#' + data.Num;
            }else if(behavior == 2) {
                expandThread(parseInt($('#qr-thread').val()), function(){
                    Post(data.Num).highlight();
                    scrollToPost(data.Num);
                });
            }
        }else{
            var highlight_num = data.Num;
            updatePosts(function(data){
                if(Favorites.isFavorited(window.thread.id)) Favorites.setLastPost(data.data, window.thread.id);
                Post(highlight_num).highlight();
            });
        }
        resetInputs();
    }else if(data.Status && data.Status == 'Redirect') {
        var num = data.Target;
        $alert('Тред №' + num + ' успешно создан');

        window.location.href = '/' + window.board + '/res/' + num + '.html';
    }else{
        $alert('Ошибка постинга');
    }

    on_complete();
};

var on_complete = function() {
    busy = false;
    renderSendingDone();
    loadCaptcha();
};

var resetInputs = function() {
    $('#subject').val('');
    $('#shampoo').val('');
    $('#qr-shampoo').val('');
    $('.message-byte-len').html($('#settings-comment-length').val());
    if(window.FormFiles) window.FormFiles.reset();
};

var saveToStorage = function() {
    Store.set('thread.postform.name', $('#name').val());
    Store.set('thread.postform.email', $('#e-mail').val());
    var icon = $('.anoniconsselectlist').val();
    if(icon) Store.set('thread.postform.icon.' + window.thread.board, icon);
};

var validator_error = window.postform_validator_error = function(id, msg) {
    var el = $('#' + id);
    var qr_el = $('#qr-' + id);

    if(msg) $alert(msg);

    el.addClass('error');
    qr_el.addClass('error');
    (activeForm.attr('id') == 'qr-shampoo') ? qr_el.focus() : el.focus();
};

var validateForm = function(is_qr) {
    var captcha = $('#captcha-value');
    var len = unescape(encodeURIComponent($('#shampoo').val())).length;
    var max_len = parseInt($('#settings-comment-length').val());

    if($('input[name=thread]').val()=='0' && window.FormFiles && window.FormFiles.max && !window.FormFiles.count && !is_qr) return $alert('Для создания треда загрузите картинку');
    if(captcha.length && !captcha.val()) return validator_error('captcha-value', 'Вы не ввели капчу');
    if(len > max_len) return validator_error('shampoo', 'Максимальная длина сообщения ' + max_len + ' <b>байт</b>, вы ввели ' + len);
    if(!len && window.FormFiles && window.FormFiles.max && !window.FormFiles.count) return validator_error('shampoo', 'Вы ничего не ввели в сообщении');

    $('.error').removeClass('error');

    return true;
};

// =====================================

String.prototype.contains = function(substring) {
  return this.indexOf(substring) !== -1;
};

String.prototype.icontains = function(substring) {
  return this.toLowerCase().contains(substring.toLowerCase());
};

function isFormDataAvailable() {
  return window.FormData !== undefined;
}

// This is a slightly modified 'sendForm' function from "makaba/swag.js"
// It takes FormData object instead of form and it doesn't call the 'renderSending' function
function postFormData(formData) {
  busy = true;

  request = $.ajax({
    url: '/makaba/posting.fcgi?json=1',
    type: 'POST',
    dataType: 'json', // The type of data that we're expecting back from the server
    data: formData,

    xhr: function() { // Custom XMLHttpRequest
      var myXhr = $.ajaxSettings.xhr();
      if (myXhr.upload) { // Check if upload property exists
        myXhr.upload.addEventListener('progress', progressHandling, false); // For handling the progress of the upload
      }
      return myXhr;
    },

    // Ajax events
    success: on_send_success,
    error: on_send_error,

    // Options to tell jQuery not to process data or worry about content-type

    // tbh, it seems that there's no need to pass the 'cache' parameter,
    // 'cause the documentation says the following:
    // "Setting cache to false will only work correctly with HEAD and GET requests.
    // It works by appending "_={timestamp}" to the GET parameters.
    // The parameter is not needed for other types of requests,
    // except in IE8 when a POST is made to a URL that has already been requested by a GET"
    // But let's set it anyway, because "makaba/swag.js" does it
    cache: false,
    contentType: false,
    processData: false
  });
}

function onPostFormSubmit() {
  if (!isFormDataAvailable()) {
    return;
  }

  if (busy) {
    request.abort();
    return;
  }

  renderSending();
  saveToStorage();

  var postForm = $(this);
  var formData = new FormData(postForm[0]);

  // TODO: Add 'validateForm' function call

  var comment = formData.get('comment');
  if (comment.icontains('randompic') ||
      comment.icontains('рандомпик') ||
      comment.icontains('пикрандом')) {
      function messagesEventListener() {
        if (event.data.type === 'take_random_pic') {
          // Let's remove the event listener immediately 'cause we want to use it for the
          // single request-response session only
          window.removeEventListener('message', messagesEventListener);

          var randomPic = event.data.randomPic;
          if (randomPic === null) {
            on_send_error({
              statusText: 'Не получилось найти рандомное изображение'
            });
            return;
          }

          // By default Chome does not support the append function,
          // although we can enable that functionality by activating an experimental flag
          // on chrome://flags/#enable-experimental-web-platform-features page
          // (https://developer.mozilla.org/en-US/docs/Web/API/FormData)
          // "Chrome support for methods other than append
          // is currently behind the "Enable Experimental Web Platform Features" flag"
          formData.append('image1', randomPic, randomPic.name);

          postFormData(formData);
        }
      }

      window.addEventListener('message', messagesEventListener);
      window.postMessage({ type: 'get_random_pic' }, '*');
  } else {
    window.FormFiles.appendToForm(postForm);
    formData = new FormData(postForm[0]);
    postFormData(formData);
  }

  // Prevent the default action of the event
  // and prevent the event from bubbling up the DOM tree
  return false;
}

$('#qr-cancel-upload').off('click').on('click', function() {
  request.abort();
});

forms.off('submit').on('submit', onPostFormSubmit);
