// Scroll to the bottom of this file to find the document-ready "entry point"

function base64toBlob(b64Data, contentType, sliceSize) {
  contentType = contentType || '';
  sliceSize = sliceSize || 512;

  var byteCharacters = atob(b64Data);
  var byteArrays = [];

  for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    var slice = byteCharacters.slice(offset, offset + sliceSize);

    var byteNumbers = new Array(slice.length);
    for (var i = 0; i < slice.length; ++i) {
      byteNumbers[i] = slice.charCodeAt(i);
    }

    var byteArray = new Uint8Array(byteNumbers);

    byteArrays.push(byteArray);
  }

  var blob = new Blob(byteArrays, {type: contentType});
  return blob;
}

function dataURLToBlob(dataURL) {
  // Format: data:[<MIME-type>][;charset=<encoding>][;base64],<data>

  var tokens = dataURL.split(',');
  if (tokens.length !== 2) {
    return null;
  }

  var headers = tokens[0].split(/[:;]+/);
  if (headers[headers.length - 1] !== 'base64') {
    return null;
  }

  var mimeType = headers[1];
  var base64 = tokens[1];

  return base64toBlob(base64, mimeType);
}

function imgURLToDataURL(url, outputFormat, callback) {
  var img = new Image();
  img.crossOrigin = 'Anonymous';
  img.onload = function() {
    var canvas = document.createElement('canvas');
    var ctx = canvas.getContext('2d');
    var dataURL;
    canvas.height = this.height;
    canvas.width = this.width;
    ctx.drawImage(this, 0, 0);
    dataURL = canvas.toDataURL(outputFormat);
    callback(dataURL);
    canvas = null;
  };
  img.onerror = function() {
    callback(null);
  };
  img.src = url;
}

function getRandomItem(arr) {
  return arr[Math.floor(Math.random() * arr.length)];
}

function getRandomPic(callback) {
  var url = generateRandomURL();
  $.get(url, function(data) {
    var doc = $(data);

    // This page contains shrinked versions of images
    // For their full-sized equivalents, we have to do some more work

    var imgsLinks = doc.find('.dg_u a'); // .dg_u is a div containers that holds the requested images
    if (imgsLinks.length === 0) {
      getRandomPic(callback);
      return;
    }

    var randomImgLink = $(getRandomItem(imgsLinks));

    // Custom 'm' attribute contains JS object with a lot of useful info like this:
    // =============================
    // m="{ ns:"images", k:"5019",
    // mid:"2AB97E81BDE91B5E71D07B8BD9C6F694FC1F18EF",
    // md5:"cefe06b1db331842de7b2bb60c3ad2f3",
    // surl:"http://themamsshow.over-blog.com/article-carots-are-green-98812097.html",
    // imgurl:"http://img.over-blog.com/460x258/5/52/40/30/DSCF2789.JPG",
    // tid:"OIP.Mcefe06b1db331842de7b2bb60c3ad2f3o0",
    // ow:"300",docid:"608036592599762859",oh:"168",tft:"62"
    // }"
    // =============================
    // The most interesting one for us at the moment is the 'imgurl'
    // that points to the original image location
    var randomImgInfoStr = randomImgLink.attr('m');
    if (randomImgInfoStr === undefined) {
      getRandomPic(callback);
      return;
    }

    var randomImgInfo = JSON5.parse(randomImgInfoStr);
    // To be able to access sites that are not CORS-enabled,
    // let's append "http://crossorigin.me" to the beginning of the image URL
    imgURLToDataURL('https://crossorigin.me/' + randomImgInfo.imgurl, 'image/jpeg', function(dataURL) {
      if (dataURL === null) {
        getRandomPic(callback);
        return;
      }

      var blob = dataURLToBlob(dataURL);
      if (blob === null) {
        getRandomPic(callback);
        return;
      }

      var file = new File([blob], 'image.jpg');
      callback(file);
    });
  }).fail(function() {
    callback(null);
  });
}

// Injects the specified file to the 'head' tag
// Note that it will not wait until the script finish its execution
function injectJSFileToHead(path) {
  var e = document.createElement('script');
  e.src = path;
  (document.head || document.documentElement).appendChild(e);
}

// Checks whether the current page contains #postform or #qr-postform elements
// that can be used to post new messages on 2ch.hk
function pageContainsPostForm() {
  return $('#postform, #qr-postform').length > 0;
}

function messagesEventListener(event) {
  if (event.data.type === 'get_random_pic') {
    getRandomPic(function(randomPic) {
      window.postMessage({ type: 'take_random_pic', randomPic: randomPic }, '*');
    });
  }
}

function onDOMReady() {
  // If there's no post form on the page
  // (for example, on the main page -- https://2ch.hk/),
  // we don't need to do anything
  if (!pageContainsPostForm()) {
    return;
  }

  // Content scripts execute in a special environment called an isolated world,
  // as stated in the documentation
  // (https://developer.chrome.com/extensions/content_scripts#execution-environment)
  // As a result, they don't have an access
  // to JavaScript variables or functions created by the page,
  // so we can't just unbind the default post forms' 'submit' event listeners via
  // $('#postform, #qr-postform').off('submit');
  // because the content script doesn't know about them

  // But here comes a small hack -- we can inject JS code to the 'head' tag
  // and do all necessary actions from there in the page-related environment

  // However, we can't place all the extension-related code into the 'head' tag
  // because then we will be unable to make cross-domain requests to the bing.com
  // (and YQL doesn't help in this case -- it'll return pages w/o images)

  // To sum the things up:
  // Let's inject as much of our code as possible to the 'head' tag,
  // leaving only getRandomPic-related stuff here
  // Then we will communicate between the content script and the head-injected code via
  // the official way described in the documentation --
  // window.addEventListener and window.postMessage functions
  // (https://developer.chrome.com/extensions/content_scripts#host-page-communication)

  injectJSFileToHead(chrome.extension.getURL('src/js/embedded.js'));

  // Wait for the 'get_random_pic' requests
  window.addEventListener('message', messagesEventListener);
}

$(function() {
  onDOMReady();
});
